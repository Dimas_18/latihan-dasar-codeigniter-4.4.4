<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\ModelMahasiswa;
use App\Models\ModelWilayah;

class Mahasiswa extends BaseController
{
    public $ModelMahasiswa;
    public $ModelWilayah;

    public function __construct()
    {
        $this->ModelMahasiswa = new ModelMahasiswa();
        $this->ModelWilayah = new ModelWilayah();
    }

    public function index()
    {
        $data = [
            'judul' => 'Mahasiswa',
            'page' => 'mahasiswa/v_mahasiswa',
            'menu' => 'masterdata',
            'submenu' => 'mahasiswa',
            'mhs' => $this->ModelMahasiswa->AllData(),
        ];
        return view('v_template', $data);
    }

    public function Tambah()
    {
        $data = [
            'judul' => 'Tambah Mahasiswa',
            'page' => 'mahasiswa/v_tambah',
            'menu' => 'masterdata',
            'submenu' => 'mahasiswa',
            'fakultas' => $this->ModelMahasiswa->AllFakultas(),
            'prodi' => $this->ModelMahasiswa->AllProdi(),
            'provinsi' => $this->ModelWilayah->AllProvinsi(),
        ];
        return view('v_template', $data);
    }

    public function InsertData()
    {

        if ($this->validate([
            'nim' => [
                'label' => 'NIM',
                'rules' => 'required|is_unique[tbl_mahasiswa.nim]',
                'errors' => [
                    'required' => '{field} wajib diisi!',
                    'is_unique' => '{field} sudah ada, input data yang lain!'
                ]
            ],
            'nama_mahasiswa' => [
                'label' => 'Nama Mahasiswa',
                'rules' => 'required|is_unique[tbl_mahasiswa.nama_mahasiswa]',
                'errors' => [
                    'required' => '{field} wajib diisi!',
                    'is_unique' => '{field} sudah ada, input data yang lain!'
                ]
            ],
            'tempat_lahir' => [
                'label' => 'Tempat Lahir',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'tgl_lahir' => [
                'label' => 'Tanggal Lahir',
                'rules' => 'required|is_unique[tbl_mahasiswa.tgl_lahir]',
                'errors' => [
                    'required' => '{field} wajib diisi!',
                    'is_unique' => '{field} sudah ada, input data yang lain!'
                ]
            ],
            'jenis_kelamin' => [
                'label' => 'Jenis Kelamin',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_fakultas' => [
                'label' => 'Fakultas',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_prodi' => [
                'label' => 'Program Studi',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'foto' => [
                'label' => 'Foto Mahasiswa',
                'rules' => 'uploaded[foto]|max_size[foto,2048]|mime_in[foto,image/png,image/jpeg,image/jpg]',
                'errors' => [
                    'uploaded' => '{field} wajib diisi!',
                    'max_size' => 'Ukuran {field} maksimal 1024 KB !',
                    'mime_in' => 'Format {field} harus PNG dan JPEG !'
                ]
            ],
            'id_provinsi' => [
                'label' => 'Provinsi',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_kabupaten' => [
                'label' => 'Kabupaten/Kota',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_kecamatan' => [
                'label' => 'Kecamatan',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ]
        ])) {
            $foto = $this->request->getFile('foto');
            $nama_file = $foto->getRandomName();

            $data = [
                'nim' => $this->request->getPost('nim'),
                'nama_mahasiswa' => $this->request->getPost('nama_mahasiswa'),
                'tempat_lahir' => $this->request->getPost('tempat_lahir'),
                'tgl_lahir' => $this->request->getPost('tgl_lahir'),
                'jenis_kelamin' => $this->request->getPost('jenis_kelamin'),
                'id_fakultas' => $this->request->getPost('id_fakultas'),
                'id_prodi' => $this->request->getPost('id_prodi'),
                'foto' => $nama_file,
                'id_provinsi' => $this->request->getPost('id_provinsi'),
                'id_kabupaten' => $this->request->getPost('id_kabupaten'),
                'id_kecamatan' => $this->request->getPost('id_kecamatan'),
            ];
            $foto->move('foto', $nama_file);
            $this->ModelMahasiswa->InsertData($data);
            session()->setFlashdata('insert', 'Data berhasil ditambahkan!');
            return redirect()->to(base_url('Mahasiswa'));
        } else {
            session()->setFlashdata('errors', \Config\Services::validation()->getErrors());
            return redirect()->to(base_url('Mahasiswa/Tambah'))->withInput('validation', \Config\Services::validation());
        }
    }

    public function Edit($id_mahasiswa)
    {
        $data = [
            'judul' => 'Edit Mahasiswa',
            'page' => 'mahasiswa/v_edit',
            'menu' => 'masterdata',
            'submenu' => 'mahasiswa',
            'mhs' => $this->ModelMahasiswa->DetailData($id_mahasiswa),
            'fakultas' => $this->ModelMahasiswa->AllFakultas(),
            'prodi' => $this->ModelMahasiswa->AllProdi(),
            'provinsi' => $this->ModelWilayah->AllProvinsi(),
        ];
        return view('v_template', $data);
    }

    public function UpdateData($id_mahasiswa)
    {

        if ($this->validate([
            'nama_mahasiswa' => [
                'label' => 'Nama Mahasiswa',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'tempat_lahir' => [
                'label' => 'Tempat Lahir',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'tgl_lahir' => [
                'label' => 'Tanggal Lahir',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'jenis_kelamin' => [
                'label' => 'Jenis Kelamin',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_fakultas' => [
                'label' => 'Fakultas',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_prodi' => [
                'label' => 'Program Studi',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'foto' => [
                'label' => 'Foto Mahasiswa',
                'rules' => 'max_size[foto,2048]|mime_in[foto,image/png,image/jpeg,image/jpg]',
                'errors' => [
                    'uploaded' => '{field} wajib diisi!',
                    'max_size' => 'Ukuran {field} maksimal 1024 KB !',
                    'mime_in' => 'Format {field} harus PNG dan JPEG !'
                ]
            ],
            'id_provinsi' => [
                'label' => 'Provinsi',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_kabupaten' => [
                'label' => 'Kabupaten/Kota',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ],
            'id_kecamatan' => [
                'label' => 'Kecamatan',
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} wajib diisi!'
                ]
            ]

        ])) {
            $mhs = $this->ModelMahasiswa->DetailData($id_mahasiswa);
            $foto = $this->request->getFile('foto');
            if ($foto->getError() == 4) {
                $nama_file = $mhs['foto'];
            } else {
                $nama_file = $foto->getRandomName();
                $foto->move('foto', $nama_file);
            }
            $data = [
                'id_mahasiswa' => $id_mahasiswa,
                'nama_mahasiswa' => $this->request->getPost('nama_mahasiswa'),
                'tempat_lahir' => $this->request->getPost('tempat_lahir'),
                'tgl_lahir' => $this->request->getPost('tgl_lahir'),
                'jenis_kelamin' => $this->request->getPost('jenis_kelamin'),
                'id_fakultas' => $this->request->getPost('id_fakultas'),
                'id_prodi' => $this->request->getPost('id_prodi'),
                'foto' => $nama_file,
                'id_provinsi' => $this->request->getPost('id_provinsi'),
                'id_kabupaten' => $this->request->getPost('id_kabupaten'),
                'id_kecamatan' => $this->request->getPost('id_kecamatan'),
            ];
            $this->ModelMahasiswa->UpdateData($data);
            session()->setFlashdata('update', 'Data berhasil diubah!');
            return redirect()->to(base_url('Mahasiswa'));
        } else {
            session()->setFlashdata('errors', \Config\Services::validation()->getErrors());
            return redirect()->to(base_url('Mahasiswa/Edit/' . $id_mahasiswa))->withInput('validation', \Config\Services::validation());
        }
    }

    public function Delete($id_mahasiswa)
    {
        $mhs = $this->ModelMahasiswa->DetailData($id_mahasiswa);
        if ($mhs['foto'] <> '') {
            unlink('foto/' . $mhs['foto']);
        }
        $data = [
            'id_mahasiswa' => $id_mahasiswa,
        ];
        $this->ModelMahasiswa->DeleteData($data);
        session()->setFlashdata('delete', 'Data berhasil dihapus!');
        return redirect()->to(base_url('Mahasiswa'));
    }

    public function Kabupaten()
    {
        $id_provinsi = $this->request->getPost('id_provinsi');
        $kab = $this->ModelWilayah->AllKabupaten($id_provinsi);
        echo '<option value="">--Pilih Kabupaten/Kota--</option>';
        foreach ($kab as $key => $k) {
            echo "<option value=" . $k['id_kabupaten'] . ">" . $k['nama_kabupaten'] . "</option>";
        }
    }

    public function Kecamatan()
    {
        $id_kabupaten = $this->request->getPost('id_kabupaten');
        $kec = $this->ModelWilayah->AllKecamatan($id_kabupaten);
        echo '<option value="">--Pilih Kecamatan--</option>';
        foreach ($kec as $key => $k) {
            echo "<option value=" . $k['id_kecamatan'] . ">" . $k['nama_kecamatan'] . "</option>";
        }
    }

    public function Print()
    {
        $data = [
            'judul' => 'Mahasiswa',
            'menu' => 'masterdata',
            'submenu' => 'mahasiswa',
            'mhs' => $this->ModelMahasiswa->AllData(),
        ];
        return view('Mahasiswa/v_print', $data);
    }
}
