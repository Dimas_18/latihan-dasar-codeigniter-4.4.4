<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Dosen extends BaseController
{
    public function index()
    {
        $data = [
            'judul' => 'Dosen',
            'page' => 'v_dosen',
            'menu' => 'masterdata',
            'submenu' => 'dosen',
        ];
        return view('v_template', $data);
    }
}
