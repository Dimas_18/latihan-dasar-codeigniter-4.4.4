<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Fakultas extends BaseController
{
    public function index()
    {
        $data = [
            'judul' => 'Fakultas',
            'page' => 'v_fakultas',
            'menu' => 'masterdata',
            'submenu' => 'fakultas',
        ];
        return view('v_template', $data);
    }
}
