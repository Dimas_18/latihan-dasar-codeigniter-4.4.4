<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Prodi extends BaseController
{
    public function index()
    {
        $data = [
            'judul' => 'Program Studi',
            'page' => 'v_prodi',
            'menu' => 'masterdata',
            'submenu' => 'prodi',
        ];
        return view('v_template', $data);
    }
}
