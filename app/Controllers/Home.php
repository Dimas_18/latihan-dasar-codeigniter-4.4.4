<?php

namespace App\Controllers;

use App\Models\ModelMahasiswa;

class Home extends BaseController
{
    public $ModelMahasiswa;

    public function __construct()
    {
        $this->ModelMahasiswa = new ModelMahasiswa();
    }
    
    public function index()// : string
    {
        // echo '<h1>Halaman Utama Web</h1>';
        // return view('welcome_message');
        $data = [
            'judul' => 'Dashboard',
            'page' => 'v_home',
            'menu' => 'dashboard',
            'submenu' => '',
            'mahasiswa' => $this->ModelMahasiswa->AllData(),
            'fakultas' => $this->ModelMahasiswa->AllFakultas(),
        ];
        return view('v_template', $data);
    }

    /*
    public function Artikel()
    {
        // echo '<h1>Halaman Artikel</h1>';
        $data = [
            'page' => 'v_artikel',
        ];
        return view('v_template', $data);
    }

    public function About()
    {
        // echo '<h1>Halaman About</h1>';
        $data = [
            'page' => 'v_about',
        ];
        return view('v_template', $data);
    }
    */
}
