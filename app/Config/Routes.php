<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
// $routes->setDefaultController('Sekolah');
// $routes->setDefaultMethod('index');
$routes->setAutoRoute(true);

$routes->get('/', 'Home::index');
// $routes->get('/', 'Sekolah::index');
// $routes->get('/Home', 'Home::index'); // Di kolom address browser akan menjadi 'http://localhost/percobaan3/public/Home/' atau 'http://localhost/percobaan3/public/Home/index'
// $routes->get('/Home/Artikel', 'Home::Artikel'); // Di kolom address browser akan menjadi 'http://localhost/percobaan3/public/Home/Artikel'
// $routes->get('/Artikel', 'Home::Artikel'); // Di kolom address browser akan menjadi 'http://localhost/percobaan3/public/Artikel'
// $routes->get('/Home/About', 'Home::About');
// $routes->get('/About', 'Home::About');
// $routes->get('/Sekolah', 'Sekolah::index'); // Di kolom address browser akan menjadi 'http://localhost/percobaan3/public/Sekolah/' atau 'http://localhost/percobaan3/public/Sekolah/index'
// $routes->get('/Guru', 'Sekolah::Guru');
// $routes->get('/Siswa', 'Sekolah::Siswa');