<div class="col-md-12">
    <div class="card card-outline card-primary">
        <div class="card-header">
            <h3 class="card-title"><?= $judul; ?></h3>

            <div class="card-tools">

                <a target="_blank" href="<?= base_url('Mahasiswa/Print'); ?>" class="btn btn-flat btn-primary btn-xm">
                    <i class="fas fa-print"></i> Print
                </a>
                <a href="<?= base_url('Mahasiswa/Tambah'); ?>" class="btn btn-flat btn-primary btn-xm">
                    <i class="fas fa-plus"></i> Tambah
                </a>

            </div>

            <!-- /.card-tools -->
        </div>
        <div class="card-body">
            <?php
            if (session()->getFlashdata('insert')) {
                echo '<div class="alert alert-success">';
                echo session()->getFlashdata('insert');
                echo '</div>';
            }
            if (session()->getFlashdata('update')) {
                echo '<div class="alert alert-primary">';
                echo session()->getFlashdata('update');
                echo '</div>';
            }
            if (session()->getFlashdata('delete')) {
                echo '<div class="alert alert-warning">';
                echo session()->getFlashdata('delete');
                echo '</div>';
            }
            ?>
            <table class="table table-border table-sm">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Tempat, Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>Fakultas</th>
                        <th>Program Studi</th>
                        <th>Foto</th>
                        <th>Alamat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($mhs as $key => $value) { ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $value['nim']; ?></td>
                            <td><?= $value['nama_mahasiswa']; ?></td>
                            <!-- Format 'hari(d/D),bulan(m/M),tahun(y/Y)'. Jika format simbol berbentuk huruf kecil maka akan menampilkan data angka, sebaliknya simbol berbentuk huruf kapital maka akan menampilkan data nama -->
                            <td><?= $value['tempat_lahir']; ?>, <?= date('d:M-Y', strtotime($value['tgl_lahir'])); ?></td>
                            <!-- Simbol '==' artinya 'adalah', '?' artinya 'maka' dan ':' artinya 'selain dari itu' -->
                            <td><?= $value['jenis_kelamin'] == 'L' ? 'Laki-Laki' : 'Perempuan'; ?></td>
                            <td><?= $value['fakultas']; ?></td>
                            <td><?= $value['prodi']; ?></td>
                            <td><img src="<?= base_url('foto/' . $value['foto']); ?>" width="150px" height="180px"></td>
                            <td><?= $value['nama_kecamatan']; ?>, <?= $value['nama_kabupaten']; ?><br><?= $value['nama_provinsi']; ?></td>
                            <td>
                                <a href="<?= base_url('Mahasiswa/Edit/' . $value['id_mahasiswa']) ?>" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a>
                                <a href="<?= base_url('Mahasiswa/Delete/' . $value['id_mahasiswa']) ?>" onclick="return confirm('Yakin hapus data ?')" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>