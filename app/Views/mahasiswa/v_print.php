<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4 landscape</title>

    <!-- Normalize or reset CSS with your favorite library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

    <!-- Load paper.css for happy printing -->
    <link rel="stylesheet" href="<?= base_url(''); ?>/paper-css/paper.css">

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="A4 landscape" onload="print()">

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <section class="sheet padding-10mm">
        <center><h3>Laporan Anu</h3></center>
        <table width="100%" border="">
                <tr>
                    <th>No.</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Tempat, Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Fakultas</th>
                    <th>Program Studi</th>
                    <th>Foto</th>
                    <th>Alamat</th>
                </tr>
                <?php $no = 1;
                foreach ($mhs as $key => $value) { ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $value['nim']; ?></td>
                        <td><?= $value['nama_mahasiswa']; ?></td>
                        <!-- Format 'hari(d/D),bulan(m/M),tahun(y/Y)'. Jika format simbol berbentuk huruf kecil maka akan menampilkan data angka, sebaliknya simbol berbentuk huruf kapital maka akan menampilkan data nama -->
                        <td><?= $value['tempat_lahir']; ?>, <?= date('d:M-Y', strtotime($value['tgl_lahir'])); ?></td>
                        <!-- Simbol '==' artinya 'adalah', '?' artinya 'maka' dan ':' artinya 'selain dari itu' -->
                        <td><?= $value['jenis_kelamin'] == 'L' ? 'Laki-Laki' : 'Perempuan'; ?></td>
                        <td><?= $value['fakultas']; ?></td>
                        <td><?= $value['prodi']; ?></td>
                        <td><img src="<?= base_url('foto/' . $value['foto']); ?>" width="80px" height="90px"></td>
                        <td><?= $value['nama_kecamatan']; ?>, <?= $value['nama_kabupaten']; ?><br><?= $value['nama_provinsi']; ?></td>
                    </tr>
                <?php } ?>
        </table>
    </section>
</body>

</html>