<div class="col-md-12">
    <div class="card card-outline card-primary">
        <div class="card-header">
            <h3 class="card-title"><?= $judul; ?></h3>
        </div>
        <div class="card-body">

            <?php
            session();
            $validation = \Config\Services::validation();
            ?>

            <?php echo form_open_multipart('Mahasiswa/InsertData') ?>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">NIM</label>
                        <input type="number" value="<?= old('nim'); ?>" name="nim" class="form-control">
                        <p class="text-danger"><?= isset($errors['nim']) == isset($errors['nim']) ? validation_show_error('nim') : '' ?></p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Nama Mahasiswa</label>
                        <input type="text" value="<?= old('nama_mahasiswa'); ?>" name="nama_mahasiswa" class="form-control">
                        <p class="text-danger"><?= isset($errors['nama_mahasiswa']) == isset($errors['nama_mahasiswa']) ? validation_show_error('nama_mahasiswa') : '' ?></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Tempat Lahir</label>
                        <input value="<?= old('tempat_lahir'); ?>" name="tempat_lahir" class="form-control">
                        <p class="text-danger"><?= isset($errors['tempat_lahir']) == isset($errors['tempat_lahir']) ? validation_show_error('tempat_lahir') : '' ?></p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <input value="<?= old('tgl_lahir'); ?>" type="date" name="tgl_lahir" class="form-control">
                        <p class="text-danger"><?= isset($errors['tgl_lahir']) == isset($errors['tgl_lahir']) ? validation_show_error('tgl_lahir') : '' ?></p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="">Jenis Kelamin</label>
                <select value="<?= old('jenis_kelamin'); ?>" name="jenis_kelamin" class="form-control">
                    <option value="">--Pilih Jenis Kelamin--</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
                <p class="text-danger"><?= isset($errors['jenis_kelamin']) == isset($errors['jenis_kelamin']) ? validation_show_error('jenis_kelamin') : '' ?></p>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Fakultas</label>
                        <select value="<?= old('id_fakultas'); ?>" name="id_fakultas" class="form-control">
                            <option value="">--Pilih Fakultas--</option>
                            <?php foreach ($fakultas as $key => $value) { ?>
                                <option value="<?= $value['id_fakultas'] ?>"><?= $value['fakultas']; ?></option>
                            <?php } ?>
                        </select>
                        <p class="text-danger"><?= isset($errors['id_fakultas']) == isset($errors['id_fakultas']) ? validation_show_error('id_fakultas') : '' ?></p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Program Studi</label>
                        <select value="<?= old('id_prodi'); ?>" name="id_prodi" class="form-control">
                            <option value="">--Pilih Prodi--</option>
                            <?php foreach ($prodi as $key => $value) { ?>
                                <option value="<?= $value['id_prodi'] ?>"><?= $value['prodi']; ?></option>
                            <?php } ?>
                        </select>
                        <p class="text-danger"><?= isset($errors['id_prodi']) == isset($errors['id_prodi']) ? validation_show_error('id_prodi') : '' ?></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="">Upload Foto</label>
                        <input id="preview_gambar" type="file" name="foto" class="form-control" accept="image/*">
                        <p class="text-danger"><?= isset($errors['foto']) == isset($errors['foto']) ? validation_show_error('foto') : '' ?></p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="">Preview Foto</label>
                    <div class="form-group">
                        <img src="<?= base_url('foto/anu.jpg'); ?>" id="gambar_load" width="150px" height="150px">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Provinsi</label>
                        <select value="<?= old('provinsi'); ?>" name="id_provinsi" id="id_provinsi" class="form-control">
                            <option value="">--Pilih Provinsi--</option>
                            <?php foreach ($provinsi as $key => $value) { ?>
                                <option value="<?= $value['id_provinsi'] ?>"><?= $value['nama_provinsi']; ?></option>
                            <?php } ?>
                        </select>
                        <p class="text-danger"><?= isset($errors['id_provinsi']) == isset($errors['id_provinsi']) ? validation_show_error('id_provinsi') : '' ?></p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Kabupaten/Kota</label>
                        <select value="<?= old('kabupaten'); ?>" name="id_kabupaten" id="id_kabupaten" class="form-control">
                            
                        </select>
                        <p class="text-danger"><?= isset($errors['id_kabupaten']) == isset($errors['id_kabupaten']) ? validation_show_error('id_kabupaten') : '' ?></p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="">Kecamatan</label>
                        <select value="<?= old('kecamatan'); ?>" name="id_kecamatan" id="id_kecamatan" class="form-control">
                            
                        </select>
                        <p class="text-danger"><?= isset($errors['id_kecamatan']) == isset($errors['id_kecamatan']) ? validation_show_error('id_kecamatan') : '' ?></p>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?= base_url('Mahasiswa'); ?>" class="btn btn-success">Kembali</a>

            <?php echo form_close() ?>
        </div>
    </div>
</div>

<script>
    function bacaGambar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#gambar_load').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#preview_gambar').change(function() {
        bacaGambar(this);
    });
</script>

<script>
    $(document).ready(function() {

        $("#id_provinsi").change(function(e) {
            var id_provinsi = $("#id_provinsi").val();
            $.ajax({
                type: "POST",
                url: "<?= base_url('Mahasiswa/Kabupaten'); ?>",
                data: {
                    id_provinsi: id_provinsi
                },
                success: function(response) {
                    $("#id_kabupaten").html(response);
                }
            });
        });

        $("#id_kabupaten").change(function(e) {
            var id_kabupaten = $("#id_kabupaten").val();
            $.ajax({
                type: "POST",
                url: "<?= base_url('Mahasiswa/Kecamatan'); ?>",
                data: {
                    id_kabupaten: id_kabupaten
                },
                success: function(response) {
                    $("#id_kecamatan").html(response);
                }
            });
        });

    });
</script>