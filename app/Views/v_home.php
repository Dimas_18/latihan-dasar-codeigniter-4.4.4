<div class="col-sm-6">
    <!-- PIE CHART -->
    <div class="card card-danger">
        <div class="card-header">
            <h3 class="card-title">Jenis Kelamin</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <canvas id="pieChart"></canvas>
        </div>
        <!-- /.card-body -->
    </div>
</div>

<div class="col-sm-6">
    <!-- BAR CHART -->
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">Fakultas</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <canvas id="barChart"></canvas>
        </div>
        <!-- /.card-body -->
    </div>
</div>

<?php

foreach ($mahasiswa as $key => $m) {
    $db = \Config\Database::connect();

    $jk_l = $db->table('tbl_mahasiswa')
        ->where('jenis_kelamin', 'L')
        ->countAllResults();

    $jk_p = $db->table('tbl_mahasiswa')
        ->where('jenis_kelamin', 'P')
        ->countAllResults();
} ?>

<script>
    const piectx = document.getElementById('pieChart');

    new Chart(piectx, {
        type: 'pie',
        data: {
            labels: ['Laki-Laki', 'Perempuan'],
            datasets: [{
                data: [<?= $jk_l ?>, <?= $jk_p ?>],
                backgroundColor: [
                    'rgb(54, 162, 235)',
                    'rgb(255, 99, 132)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>

<?php foreach ($fakultas as $key => $value) {
    $db = \Config\Database::connect();
    $jml[] = $db->table('tbl_mahasiswa')
        ->where('id_fakultas', $value['id_fakultas'])
        ->countAllResults();
    $fakul[] = $value['fakultas'];
} ?>

<script>
    const barctx = document.getElementById('barChart');

    new Chart(barctx, {
        type: 'bar',
        data: {
            labels: <?= json_encode($fakul) ?>,
            datasets: [{
                label: 'Jumlah Mahasiswa Berdasarkan Fakultas',
                data: <?= json_encode($jml) ?>,
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 205, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(201, 203, 207, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>