<?php

namespace App\Models;

use CodeIgniter\Model;

class ModelMahasiswa extends Model
{
    public function AllData()
    {
        return $this->db->table('tbl_mahasiswa')
        ->join('tbl_fakultas', 'tbl_fakultas.id_fakultas=tbl_mahasiswa.id_fakultas', 'left')
        ->join('tbl_prodi', 'tbl_prodi.id_prodi=tbl_mahasiswa.id_prodi', 'left')
        ->join('tbl_provinsi', 'tbl_provinsi.id_provinsi=tbl_mahasiswa.id_provinsi', 'left')
        ->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten=tbl_mahasiswa.id_kabupaten', 'left')
        ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan=tbl_mahasiswa.id_kecamatan', 'left')
        ->Get()->getResultArray();
    }

    public function InsertData($data)
    {
        $this->db->table('tbl_mahasiswa')->insert($data);
    }

    public function DetailData($id_mahasiswa)
    {
        return $this->db->table('tbl_mahasiswa')
        ->where('id_mahasiswa', $id_mahasiswa)
        ->join('tbl_provinsi', 'tbl_provinsi.id_provinsi=tbl_mahasiswa.id_provinsi', 'left')
        ->join('tbl_kabupaten', 'tbl_kabupaten.id_kabupaten=tbl_mahasiswa.id_kabupaten', 'left')
        ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan=tbl_mahasiswa.id_kecamatan', 'left')
        ->Get()->getRowArray();
    }

    public function UpdateData($data)
    {
        $this->db->table('tbl_mahasiswa')
        ->where('id_mahasiswa', $data['id_mahasiswa'])
        ->update($data);
    }

    public function DeleteData($data)
    {
        $this->db->table('tbl_mahasiswa')
        ->where('id_mahasiswa', $data['id_mahasiswa'])
        ->delete($data);
    }

    public function AllFakultas()
    {
        return $this->db->table('tbl_fakultas')
        ->Get()->getResultArray();
    }

    public function AllProdi()
    {
        return $this->db->table('tbl_prodi')
        ->Get()->getResultArray();
    }

    // protected $table            = 'modelmahasiswas';
    // protected $primaryKey       = 'id';
    // protected $useAutoIncrement = true;
    // protected $returnType       = 'array';
    // protected $useSoftDeletes   = false;
    // protected $protectFields    = true;
    // protected $allowedFields    = [];
    // protected $useTimestamps = false;
    // protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';
    // protected $validationRules      = [];
    // protected $validationMessages   = [];
    // protected $skipValidation       = false;
    // protected $cleanValidationRules = true;
    // protected $allowCallbacks = true;
    // protected $beforeInsert   = [];
    // protected $afterInsert    = [];
    // protected $beforeUpdate   = [];
    // protected $afterUpdate    = [];
    // protected $beforeFind     = [];
    // protected $afterFind      = [];
    // protected $beforeDelete   = [];
    // protected $afterDelete    = [];
}
